# Permissions for afterlife sdk services
PRODUCT_COPY_FILES += \
    vendor/afterlife/config/permissions/org.afterlife.globalactions.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/org.afterlife.globalactions.xml \
    vendor/afterlife/config/permissions/org.afterlife.hardware.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/org.afterlife.hardware.xml \
    vendor/afterlife/config/permissions/org.afterlife.health.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/org.afterlife.health.xml \
    vendor/afterlife/config/permissions/org.afterlife.livedisplay.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/org.afterlife.livedisplay.xml \
    vendor/afterlife/config/permissions/org.afterlife.profiles.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/org.afterlife.profiles.xml \
    vendor/afterlife/config/permissions/org.afterlife.settings.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/org.afterlife.settings.xml \
    vendor/afterlife/config/permissions/org.afterlife.trust.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/org.afterlife.trust.xml

# Lineage Platform Library
PRODUCT_PACKAGES += \
    org.afterlife.platform-res \
    org.afterlife.platform

# AOSP has no support of loading framework resources from /system_ext
# so the SDK has to stay in /system for now
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/framework/oat/%/org.afterlife.platform.odex \
    system/framework/oat/%/org.afterlife.platform.vdex \
    system/framework/org.afterlife.platform-res.apk \
    system/framework/org.afterlife.platform.jar

ifndef LINEAGE_PLATFORM_SDK_VERSION
  # This is the canonical definition of the SDK version, which defines
  # the set of APIs and functionality available in the platform.  It
  # is a single integer that increases monotonically as updates to
  # the SDK are released.  It should only be incremented when the APIs for
  # the new release are frozen (so that developers don't write apps against
  # intermediate builds).
  LINEAGE_PLATFORM_SDK_VERSION := 9
endif

ifndef LINEAGE_PLATFORM_REV
  # For internal SDK revisions that are hotfixed/patched
  # Reset after each LINEAGE_PLATFORM_SDK_VERSION release
  # If you are doing a release and this is NOT 0, you are almost certainly doing it wrong
  LINEAGE_PLATFORM_REV := 0
endif
