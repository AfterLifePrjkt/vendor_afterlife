# Inherit mini common AfterLife stuff
$(call inherit-product, vendor/afterlife/config/common_mini.mk)

# Required packages
PRODUCT_PACKAGES += \
    LatinIME

$(call inherit-product, vendor/afterlife/config/telephony.mk)
