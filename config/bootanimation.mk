# Boot Animation
ifeq ($(TARGET_BOOT_ANIMATION_RES),1440)
    PRODUCT_COPY_FILES += vendor/afterlife/bootanimation/1440.zip:system/media/bootanimation.zip
else ifeq ($(TARGET_BOOT_ANIMATION_RES),1080)
    PRODUCT_COPY_FILES += vendor/afterlife//bootanimation/1080.zip:system/media/bootanimation.zip
else ifeq ($(TARGET_BOOT_ANIMATION_RES),720)
    PRODUCT_COPY_FILES += vendor/afterlife/bootanimation/720.zip:system/media/bootanimation.zip
else
    ifeq ($(TARGET_BOOT_ANIMATION_RES),)
        $(warning "AfterLife: TARGET_BOOT_ANIMATION_RES is undefined, assuming 1080p")
    else
        $(warning "AfterLife: Current bootanimation res is not supported, forcing 1080p")
    endif
    PRODUCT_COPY_FILES += vendor/afterlife/bootanimation/1080.zip:system/media/bootanimation.zip
endif
