# Inherit common AfterLife stuff
$(call inherit-product, vendor/afterlife/config/common.mk)

# Inherit AfterLife atv device tree
$(call inherit-product, device/afterlife/atv/afterlife_atv.mk)

# AOSP packages
PRODUCT_PACKAGES += \
    LeanbackIME

# AfterLife packages
PRODUCT_PACKAGES += \
    AfterCustomizer

PRODUCT_PACKAGE_OVERLAYS += vendor/afterlife/overlay/tv
