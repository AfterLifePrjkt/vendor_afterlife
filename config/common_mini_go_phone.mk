# Set AfterLife specific identifier for Android Go enabled products
PRODUCT_TYPE := go

# Inherit mini common AfterLife stuff
$(call inherit-product, vendor/afterlife/config/common_mini_phone.mk)
