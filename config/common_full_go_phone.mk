# Set AfterLife specific identifier for Android Go enabled products
PRODUCT_TYPE := go

# Inherit full common AfterLife stuff
$(call inherit-product, vendor/afterlife/config/common_full_phone.mk)
